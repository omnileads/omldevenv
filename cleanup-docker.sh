#!/bin/bash

set -e
set -u
set -o pipefail

echo "# Prune images"
docker image prune -f

echo "# Cleaning images by tag"
for img in $(docker images --format "{{.Repository}}:{{.Tag}}" | grep -E ^freetechsolutions/ | grep -v '<none>' | grep -v 'cache-this-image'); do
	docker tag $img ${img}-cache-this-image
	docker rmi --no-prune $img
done

echo "# Cleaning images with tag 'none'"
for img in $(docker images --format "{{.Repository}}:{{.Tag}}:{{.ID}}" | grep -E ^freetechsolutions/ | grep ':<none>:') ; do
	docker rmi --no-prune $(echo $img | cut -d: -f3)
done

echo "# Cleaning volumes"
for vol in $(docker volume ls --format "{{.Name}}" | grep -E '^devenv_') ; do
	docker volume rm $vol
done

echo "# Non-user files"
# find . \! -uid $(id -u) -print0 | xargs -0 sudo rm -rfv
find . \! -uid $(id -u)
