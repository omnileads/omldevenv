#!/bin/bash

# values: linux or mac
OS=linux
ASTERISK_AUDIO_PROMPTS=https://downloads.asterisk.org/pub/telephony/sounds/asterisk-core-sounds-en-alaw-current.tar.gz
OMNILEADS_AUDIO_PROMPTS=https://fts-public-packages.s3-sa-east-1.amazonaws.com/asterisk/asterisk-oml-sounds-current.tar.gz

cd ../

if [ "$OS" == "mac" ]; then
  brew install minio/stable/mc
else
  curl https://dl.min.io/client/mc/release/linux-amd64/mc --create-dirs -o $HOME/minio-binaries/mc
  chmod +x $HOME/minio-binaries/mc
  export PATH=$PATH:$HOME/minio-binaries/
fi

echo "***[OML devenv] Cloning the repositories of modules"
repositories=("acd" "kamailio" "nginx" "pgsql" "redis" "rtpengine" "haproxy")
for i in "${repositories[@]}"; do
  if [ ! -d "oml${i}" ]; then
    if [ "$1" == "git=https" ]; then
      git clone https://gitlab.com/omnileads/oml$i.git
    else
      git clone git@gitlab.com:omnileads/oml$i.git
    fi
  else
    echo "***[OML devenv] $i repository already cloned"
  fi
done

if [ "$1" == "git=https" ]; then
git clone https://gitlab.com/omnileads/omnileads-websockets.git omlwebsockets
git clone https://gitlab.com/omnileads/ominicontacto omlapp
else
git clone git@gitlab.com:omnileads/omnileads-websockets.git omlwebsockets
git clone git@gitlab.com:omnileads/ominicontacto omlapp
fi

echo "***[OML devenv] All repositories were cloned in $(pwd)"

echo "Download en Asterisk sounds"
mkdir -p ./omlacd/sounds/asterisk
curl -s $ASTERISK_AUDIO_PROMPTS | tar xvz -C ./omlacd/sounds/asterisk
rm -f asterisk-core-sounds-en-alaw-current.tar.gz

echo "Download OMniLeads sounds"
mkdir -p ./omlacd/sounds/oml
curl -s $OMNILEADS_AUDIO_PROMPTS | tar xvz -C ./omlacd/sounds/oml
rm -f asterisk-oml-sounds-current.tar.gz

repositories=("acd" "kamailio" "nginx" "pgsql" "redis" "rtpengine" "haproxy" "omlwebsockets" "omlapp")
for i in "${repositories[@]}"; do
cd oml${i} && git checkout develop && cd ..
done

cd omldevenv/
cp .env.template .env
docker-compose up -d

mc alias set MINIO http://localhost:9000 minio s3minio123
mc mb MINIO/devenv
mc admin user add MINIO devenv s3devenv123
mc admin policy set MINIO readwrite user=devenv

docker-compose down
docker-compose up -d
