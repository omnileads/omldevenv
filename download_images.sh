#!/bin/bash

echo "***[OML devenv] Pulling the latest images of services"
services=("acd" "app" "dialer" "kam" "nginx" "pgsql" "pbxemulator" "oml-websockets" "omlapp-builder" "")
for i in "${services[@]}"; do
  docker pull freetechsolutions/oml$i:develop
done

  docker pull redislabs/redisgears:1.0.9
  quay.io/minio/minio
