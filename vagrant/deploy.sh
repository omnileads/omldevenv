#!/bin/bash

ASTERISK_AUDIO_PROMPTS=https://downloads.asterisk.org/pub/telephony/sounds/asterisk-core-sounds-en-alaw-current.tar.gz
OMNILEADS_AUDIO_PROMPTS=https://fts-public-packages.s3-sa-east-1.amazonaws.com/asterisk/asterisk-oml-sounds-current.tar.gz

echo -e "\n"
echo "##################################################################"
echo "##          Welcome to OMniLeads devenv deploy script           ##"
echo "##################################################################"
echo ""


if [ "$1" == "docker=true" ]; then
sudo apt-get update

sudo apt install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/docker-archive-keyring.gpg

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" -y

sudo apt-get update
sudo apt-get -y install docker-ce docker-ce-cli containerd.io

sudo systemctl enable docker.service
sudo systemctl enable containerd.service
sudo systemctl start docker.service
sudo systemctl start containerd.service

sudo  curl -L "https://github.com/docker/compose/releases/download/1.28.2/docker-compose-$(uname -s)-$(uname -m)" \
    -o /usr/local/bin/docker-compose
sudo  chmod +x /usr/local/bin/docker-compose
sudo  ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

sudo usermod -aG docker $USER
fi

if [ "$3" == "mc=true" ]; then
curl https://dl.min.io/client/mc/release/linux-amd64/mc --create-dirs -o $HOME/minio-binaries/mc

chmod +x $HOME/minio-binaries/mc
export PATH=$PATH:$HOME/minio-binaries/
fi

# cd ../../../..
echo "***[OML devenv] Cloning the repositories of modules"
repositories=("acd" "kamailio" "nginx" "pgsql" "redis" "rtpengine" "haproxy")
for i in "${repositories[@]}"; do
  if [ ! -d "oml${i}" ]; then
    if [ "$2" == "https" ]; then
      git clone https://gitlab.com/omnileads/oml$i.git
    else
      git clone git@gitlab.com:omnileads/oml$i.git
    fi
  else
    echo "***[OML devenv] $i repository already cloned"
  fi
done

if [ "$2" == "https" ]; then
git clone https://gitlab.com/omnileads/omnileads-websockets.git omlwebsockets
git clone https://gitlab.com/omnileads/ominicontacto omldjango
else
git clone git@gitlab.com:omnileads/omnileads-websockets.git omlwebsockets
git clone git@gitlab.com:omnileads/ominicontacto omldjango
fi

echo "***[OML devenv] All repositories were cloned in $(pwd)"

echo "Download en Asterisk sounds"
mkdir -p ./omlacd/sounds/asterisk
curl -s $ASTERISK_AUDIO_PROMPTS | tar xvz -C ./omlacd/sounds/asterisk
rm -f asterisk-core-sounds-en-alaw-current.tar.gz

echo "Download OMniLeads sounds"
mkdir -p ./omlacd/sounds/oml
curl -s $OMNILEADS_AUDIO_PROMPTS | tar xvz -C ./omlacd/sounds/oml
rm -f asterisk-oml-sounds-current.tar.gz

cd omldjango/install/docker/devenv/
cp .env.template .env
docker-compose up -d


mc alias set MINIO http://localhost:9000 minio s3minio123
mc mb MINIO/devenv
mc admin user add MINIO devenv s3devenv123
mc admin policy set MINIO readwrite user=devenv

until curl -sk --head  --request GET https://$localhost |grep "302" > /dev/null; do echo "Environment still being installed, sleeping 80 seconds"; sleep 10; done; echo "Environment is up"


# mc alias set devenv http://localhost:9000 minio 098098ZZZ
# mc mb devenv/devenv
# mc admin user add devenv omnileads 098098XX
# mc admin policy set devenv readwrite user=omnileads
